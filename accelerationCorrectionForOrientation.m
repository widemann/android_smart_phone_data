%% correcting the acceleration for orientation. 
addpath('~/Documents/MATLAB/quaternions/')
filename = '/Users/widemann/Documents/android/myScripts/data_2013-05-14_16:36:28.csv';
[t,x,y,z,az,pitch,roll] = importSL4Afile(filename);
taxis = t/1e3; % originally in milliseconds
dt = diff(taxis);
d_az = diff(az);
d_pitch = diff(pitch);
d_roll = diff(roll);

%% create the quanternions

% rotation is about [x-axis,y-axis,z-axis]???
d_theta_dt = [d_pitch, d_roll, d_az];

omegaMagnitude = sqrt(sum(d_theta_dt.^2,2));
dthetaNormalized = bsxfun(@rdivide, d_theta_dt,omegaMagnitude);
dthetaNormalized(isnan(dthetaNormalized))=0;

thetaOverTwo = omegaMagnitude.*dt/2;
sinThetaOverTwo = sin(thetaOverTwo);
cosThetaOverTwo = cos(thetaOverTwo);

temp = bsxfun(@times,dthetaNormalized,sinThetaOverTwo)';
                                          
% the columns of this guy are the quanternions (i,j,k,rotation_angle)
% here (i,j,k) is the axis of rotation. 
deltaRotationVector = [temp; cosThetaOverTwo'];

%% put the acceleration measurements in a vector
A_measured = [x';
              y';
              z'];
          
% now undo each measurement
A_qc = zeros(size(A_measured));
A_qc(:,1) = A_measured(:,1);
start = tic;
for k = 2:numel(t)
    tic;
    v = [A_measured(:,k); 0]; 
    for j = k-1:-1:1
        q = qconj(deltaRotationVector(:,j));
        v = qmult(q,qmult(v,qconj(q)));
    end
    A_qc(:,k) = v(1:3);
    %fprintf('run-time step k: %0.2f\n',toc);
end
rt = toc(start);
fprintf('run-time: %f\n',rt);

figure, plot(taxis,A_measured',taxis,A_qc')
% figure, plot(taxis, A_measured(1,:),taxis,A_qc(1,:),'--r')
% figure, plot(taxis, A_measured(2,:),taxis,A_qc(2,:),'--r')


%% run the [0;0;g] forward based on the measurements of the orientation
% g = -9.8;
% A0 = [0;0;g];
% A_fwd = zeros(3,size(t,2));
% A_fwd(:,1) = A0; 
% for k = 2:numel(t)
%     q = deltaRotationVector(:,k-1);
%     v = [A_fwd(:,k-1); 0];
%     tmp = qmult(q,qmult(v,qconj(q))); 
%     A_fwd(:,k) = tmp(1:3);
% end





















