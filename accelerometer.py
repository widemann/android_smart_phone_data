#!/usr/bin/env python2.6
'''
http://www.smartphonedaq.com/accelerometer.page

startSensingTimed(Integer sensorNumber: 1 = All, 2 = Accelerometer, 3 = Magnetometer and 4 = Light,
Integer delayTime: Minimum time between readings in milliseconds)
Starts recording sensor data to be available for polling. Generates "sensors" events.
'''

import android 
import time 

droid = android.Android() 
dt = 10 #100ms between sensings 
endTime = 300 #sample for 300ms 
timeSensed=0 
droid.startSensingTimed(2,dt) 
while timeSensed <= endTime: 
    print droid.sensorsReadAccelerometer().result 
    time.sleep(dt/1000.0) 
    timeSensed+=dt 
droid.stopSensing()

