# -*- coding: utf-8 -*-
"""
Created on Tue May 14 11:13:32 2013

@author: widemann
"""

import numpy as np
from scipy import integrate
#from matplotlib import pylab

x = np.linspace(-2, 2, num=20)
y = x
y_int = integrate.cumtrapz(y, x, initial=0)

A = np.random.uniform(-1,1,size=(5,5))

