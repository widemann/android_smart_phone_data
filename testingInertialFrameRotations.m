%% Testing the Z-X-Z transformation for taking the inertial frame (x,y,z) 
% to the inertial frame (X,Y,Z).
% The rotation angles correspond to 
% yaw (azimuth, precession), 
% pitch (nutation),
% roll (spin)
% I'm using this reference to for defining the above angles. 
% http://ocw.mit.edu/courses/aeronautics-and-astronautics/16-07-dynamics-fall-2009/lecture-notes/MIT16_07F09_Lec29.pdf

% be careful the pitch is defined from the vertical axis down and my 
% positive rotations are counter clockwise. 
% also, the yaw is with respect to the x-axis, not the y-axis like on my
% phone. The yaw and the pitch axes are reversed for the phone.

X = @(w) [  1   0   0;
            0 cos(w) -sin(w);
            0 sin(w)  cos(w)];

Y = @(w) [ cos(w)    0  -sin(w);
            0        1    0;
           sin(w)    0   cos(w)];
        
Z = @(w) [ cos(w)  -sin(w) 0;
           sin(w)   cos(w) 0;
             0        0    1];
         
% Tait?Bryan convention,             
R = @(yaw,pitch,roll) Z(yaw)*Y(pitch)*X(roll);


initialFrame = eye(3);


yaw = pi/2;
pitch = 0;
roll = 0;

newFrame = R(yaw,pitch,roll)*initialFrame

% the answer is correct but it appears to be rotating
% the wrong direction. It's not the wrong direction!!!
% the x-z plane rotates correctly, it's just clockwise looking down from y.
yaw = 0;
pitch = pi/2;
roll = 0;

newFrame = R(yaw,pitch,roll)*initialFrame

yaw = 0;
pitch = 0;
roll = pi/2;

newFrame = R(yaw,pitch,roll)*initialFrame

%% convert to quanternions.
% http://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
% they agree with the above matrix rotations. 
addpath('~/Documents/MATLAB/quaternions/');

psi = yaw;
theta = pitch;
phi = roll;

% q0 is q3 in matlab's quanternions. normalized???
q = [sin(phi/2)*cos(theta/2)*cos(psi/2) - cos(phi/2)*sin(theta/2)*sin(psi/2);
     cos(phi/2)*sin(theta/2)*cos(psi/2) + sin(phi/2)*cos(theta/2)*sin(psi/2);
     cos(phi/2)*cos(theta/2)*sin(psi/2) - sin(phi/2)*sin(theta/2)*cos(psi/2);
     cos(phi/2)*cos(theta/2)*cos(psi/2) + sin(phi/2)*sin(theta/2)*sin(psi/2)];
 
% v = q(1:3)/norm(q(1:3));
% q(1:3) = v;
 
g = [0; 0; -9.8; 0]; 

rg = qmult(q,qmult(g,qconj(q)));






