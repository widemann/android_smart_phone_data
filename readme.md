This project demonstrates how to:

1. Collect sensor data from a smart phone

2. Write the data to file.

3. Transfer the file the computer. 

4. Use orientation correction and Kalman filtering to process the data.

Look at the .m file:

**filteringThenCorrectionForOrientation.m** 

to see an example of reading in data, correcting for the sensor orientation
and displaying the sensor's final position. 

Look at the file:

**gyroscopeNorientationWriteToFile.py** 

to see how the data is collected on the phone using SL4A. 

Look at the file:

**artificialSimulationGyroscope.m** 

to learn how the inertial frame correction is done using quaternions vs. 
rotation matrices. The quaternion approach is much nicer. 

Note: The main purpose of the code was to test whether a phone's IMU is good 
enough for making measurements. Specifically, the code maps acceleration and 
orientation to changes in position. The result is that, no, a phone's IMU is 
not good enough for our purposes. There is too much noise and a lot of 
gyroscope drift. 

Note: The code running on the phone is python. 
It uses google's SL4A project. (scripting layers for Android) 

The artificial simulations are best to start with if you want to understand 
the code. The file filteringThenCorrectionForOrientation.m shows the 
results for real data. 


