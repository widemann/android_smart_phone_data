%% artifical test oientation
% This supposes that a collection of acceleration measurements are made at 
% varying orientations. 
% We want to map the force to the standard orientation
% azimuth = 0; radians
% pitch = 0; '''
% roll = 0; '''
% no linear motion, just rotation

% force in the standard position.
g = -9.8; % (m/s^2) gravity
A0 = [0; 0; g]; % acceleration at time 0. 

N = 100; % number of measurements. 
om = randn(3,N); % az, pitch, roll % orientation measurements
om = sign(om).*mod(om,pi/2);


%% try using quanternions. 

% http://developer.android.com/guide/topics/sensors/sensors_motion.html
%       
%     final float dT = (event.timestamp - timestamp) * NS2S;
%     float omegaMagnitude = sqrt(axisX*axisX + axisY*axisY + axisZ*axisZ);
% 
%     // Normalize the rotation vector if it's big enough to get the axis
%     // (that is, EPSILON should represent your maximum allowable margin of error)
%     if (omegaMagnitude > EPSILON) {
%       axisX /= omegaMagnitude;
%       axisY /= omegaMagnitude;
%       axisZ /= omegaMagnitude;
%     }
% 
%     // Integrate around this axis with the angular speed by the timestep
%     // in order to get a delta rotation from this sample over the timestep
%     // We will convert this axis-angle representation of the delta rotation
%     // into a quataernion before turning it into the rotation matrix.
%     float thetaOverTwo = omegaMagnitude * dT / 2.0f;
%     float sinThetaOverTwo = sin(thetaOverTwo);
%     float cosThetaOverTwo = cos(thetaOverTwo);
%     deltaRotationVector[0] = sinThetaOverTwo * axisX;
%     deltaRotationVector[1] = sinThetaOverTwo * axisY;
%     deltaRotationVector[2] = sinThetaOverTwo * axisZ;
%     deltaRotationVector[3] = cosThetaOverTwo;
%   }
%   timestamp = event.timestamp;
%   float[] deltaRotationMatrix = new float[9];
%   SensorManager.getRotationMatrixFromVector(deltaRotationMatrix, deltaRotationVector);


omegaMagnitude = sqrt(sum(om.^2));
dthetaNormalized = bsxfun(@rdivide, om,omegaMagnitude);

thetaOverTwo = omegaMagnitude/2; %*dt/2;
sinThetaOverTwo = sin(thetaOverTwo);
cosThetaOverTwo = cos(thetaOverTwo);

temp = bsxfun(@times,dthetaNormalized,sinThetaOverTwo);
                                          
% the columns of this guy are the quanternions (i,j,k,rotation_angle)
% here (i,j,k) is the axis of rotation. 
deltaRotationVector = [temp; cosThetaOverTwo];

addpath('~/Documents/MATLAB/quaternions/')

A_measured = zeros(3,N);
v = [A0; 0];
for k = 1:N
    q = deltaRotationVector(:,k);
    tmp = qmult(q,qmult(v,qconj(q))); 
    A_measured(:,k) = tmp(1:3);
end

figure, 
quiver3(zeros(3,1),zeros(3,1),zeros(3,1),[1;0;0],[0;1;0],[0;0;1],'LineWidth',2);
hold on
for k = 1:N
    quiver3(0,0,0,deltaRotationVector(1,k),deltaRotationVector(2,k),deltaRotationVector(3,k));
    pause(.01);
end

% now undo each measurement
A_qc = zeros(size(A_measured));
for k = 1:N
    v = [A_measured(:,k); 0]; 
    q = qconj(deltaRotationVector(:,k));
    v = qmult(q,qmult(v,qconj(q)));
    A_qc(:,k) = v(1:3);
end

sanityCheck = bsxfun(@minus,A_qc,A0);
fprintf('max component error: %e\n', max(abs(sanityCheck(:))))


