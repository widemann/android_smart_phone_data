%% kalman filter data to estimate the position 

dt = .1;
t0 = 0;
t1 = 100;
t = t0:dt:t1;
a = 0; % acceleration
% x = Ax + aB + Nx
A = [1 dt; 0 1];
B = [dt^2/2; dt];
std_process = .05;
% Nx = std_process*B*randn
std_measure = 10; 
x = [0;0];
% measurement 
% x_meas = C x_pred + std_measure*randn 
C = [1 0];
x_meas = C*x + std_measure*randn;
% estimate the covariance matrices. 
Px = std_process^2*(B*B');% position/velocity estimated covariance matrix.
Pm = std_measure^2; %*(dt + dt^2/2); % measurement covariance (needs a (dt + dt^2/2) factor)
x_apri = [x_meas; 0]; % maybe this should be x???
P = Px; % initial estimate of covariance matrix
% compute the Kalman gain. 
K = P*C'*inv(C*P*C' + Pm);
x_est = x_apri + K*(x_meas - C*x_apri);
P = (eye(size(K,1)) - K*C)*P;
for k = 2:numel(t)
    % predict the next state
    x_apri(:,k) = A*x_est(:,k-1)+a*B;
    % estimate the next covariance matrix
    P = A*P*A' + Px;
    % create truth data, in real life this is unknown.
    x(:,k) = A*x(:,k-1) + a*B + std_process*randn*B; 
    % measure the state
    x_meas(:,k) = C*x(:,k) + std_measure*randn;
    % compute the Kalman Gain
    K = P*C'*inv(C*P*C' + Pm);
    % Kalman filter estimate
    x_est(:,k) = x_apri(:,k) + K*(x_meas(:,k) - C*x_apri(:,k));
    % update the covariance matrix 
    P = (eye(size(K,1)) - K*C)*P;
end

figure, plot(t,x(1,:),'.-k',t,x_meas,'.-r',t,x_est(1,:),'.-b')
legend('actual','measured','Kalman')
title('Kalman Filtering for Position Estimation');
xlabel('seconds')
ylabel('meters');

rms = zeros(numel(t),1);
for k = 1:numel(t)
    rms(k) = (1/k)*sqrt((sum((x(1,1:k) - x_est(1,1:k)).^2)));
end
% figure, plot(t,rms)

fprintf('rms error %f\n',rms(end));

