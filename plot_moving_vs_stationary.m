%% plot moving versus stationary

filename = '/Users/widemann/Documents/android/myScripts/data_2013-07-09_14_57_34.csv';
[t,x,y,z,az,pitch,roll] = importSL4Afile(filename);
taxis = t/1e3;

d_theta_dt = [pitch, roll, az];

omegaMagnitude = sqrt(sum(d_theta_dt.^2,2));
dthetaNormalized = bsxfun(@rdivide, d_theta_dt,omegaMagnitude);
dthetaNormalized(isnan(dthetaNormalized))=0;

thetaOverTwo = omegaMagnitude/2; %.*dt/2;
sinThetaOverTwo = sin(thetaOverTwo);
cosThetaOverTwo = cos(thetaOverTwo);

temp = bsxfun(@times,dthetaNormalized,sinThetaOverTwo)';
                                          
% the columns of this guy are the quanternions (i,j,k,rotation_angle)
% here (i,j,k) is the axis of rotation. 
deltaRotationVector = [temp; cosThetaOverTwo'];
A_measured = [x'; y'; z'];
          
% now undo each measurement
A_qc = zeros(size(A_measured));
A_qc(:,1) = A_measured(:,1);
start = tic;
for k = 1:numel(t)
    tic;
    v = [A_measured(:,k); 0]; 
    q = qconj(deltaRotationVector(:,k));
    v = qmult(q,qmult(v,qconj(q)));
    A_qc(:,k) = v(1:3);
    %fprintf('run-time step k: %0.2f\n',toc);
end
rt = toc(start);
fprintf('run-time: %f\n',rt);
moving = [taxis A_qc'];

%% stationary
filename = '/Users/widemann/Documents/android/myScripts/data_2013-05-14_16:36:28.csv';
[t,x,y,z,az,pitch,roll] = importSL4Afile(filename);
taxis = t/1e3;
d_theta_dt = [pitch, roll, az];

omegaMagnitude = sqrt(sum(d_theta_dt.^2,2));
dthetaNormalized = bsxfun(@rdivide, d_theta_dt,omegaMagnitude);
dthetaNormalized(isnan(dthetaNormalized))=0;

thetaOverTwo = omegaMagnitude/2; %.*dt/2;
sinThetaOverTwo = sin(thetaOverTwo);
cosThetaOverTwo = cos(thetaOverTwo);

temp = bsxfun(@times,dthetaNormalized,sinThetaOverTwo)';
                                          
% the columns of this guy are the quanternions (i,j,k,rotation_angle)
% here (i,j,k) is the axis of rotation. 
deltaRotationVector = [temp; cosThetaOverTwo'];
A_measured = [x'; y'; z'];
          
% now undo each measurement
A_qc = zeros(size(A_measured));
A_qc(:,1) = A_measured(:,1);
start = tic;
for k = 1:numel(t)
    tic;
    v = [A_measured(:,k); 0]; 
    q = qconj(deltaRotationVector(:,k));
    v = qmult(q,qmult(v,qconj(q)));
    A_qc(:,k) = v(1:3);
    %fprintf('run-time step k: %0.2f\n',toc);
end
rt = toc(start);
fprintf('run-time: %f\n',rt);

stationary = [taxis x y z];

C = {'x','y','z'};
for k = 2:4
    figure, plot(stationary(:,1),stationary(:,k),moving(:,1),moving(:,k))
    title(['acceleration ' C{k-1}]);
end


