#!/usr/bin/env python2.6
'''
http://www.smartphonedaq.com/accelerometer.page

startSensingTimed(Integer sensorNumber: 1 = All, 2 = Accelerometer, 3 = Magnetometer and 4 = Light,
Integer delayTime: Minimum time between readings in milliseconds)
Starts recording sensor data to be available for polling. Generates "sensors" events.
'''

import android 
import time 
import os, sys
import math

droid = android.Android() 
dt = 10 #10ms between sensings 
endTime = 30 #sample for 30ms 
timeSensed=0 
droid.startSensingTimed(2,dt) 
#nsamps = ceil(endTime/dt)
data = open('data.csv','w')
data.write("milliseconds,\t x (m/s^2),\t y,\t z\n")
while timeSensed <= endTime: 
    print droid.sensorsReadAccelerometer().result 
    x,y,z = droid.sensorsReadAccelerometer().result
    # I'm getting disagreement between the screen print out and the file.
    # sometimes the first few (x,y,z) sample says that they are None
    # But when I print them out as strings they are numbers. 
    # uncomment the code if you don't want to throw the "None" samples away
    # data.write("%f,\t" %timeSensed)
    # data.write("%s,\t %s,\t %s\n" %(x,y,z))

    if x != None:
        data.write("%f,\t%f,\t %f,\t %f\n" %(timeSensed,x,y,z))

    time.sleep(dt/1000.0) 
    timeSensed+=dt 
droid.stopSensing()
data.close()
