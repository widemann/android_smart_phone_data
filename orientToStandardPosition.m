%% Processing Steps:
% 1. load the data.
% 2. Remove the first T seconds
% 3. boxcar filter.
% 4. correct the acceleration for orientation. 
% 5. integrate and plot results. 

addpath('~/Documents/MATLAB/quaternions/')
filename = '/Users/widemann/Documents/android/myScripts/data_2013-05-14_16:36:28.csv';
%filename = '/Users/widemann/Documents/android/myScripts/data_2013-07-09_14_57_34.csv';
[t,x,y,z,az,pitch,roll] = importSL4Afile(filename);
taxis = t/1e3; % originally in milliseconds
t_thresh = .5;
t_inds = taxis > t_thresh;
taxis = taxis(t_inds);
x = x(t_inds);
y = y(t_inds);
z = z(t_inds);
az = az(t_inds);
pitch = pitch(t_inds);
roll = roll(t_inds);
dt = diff(taxis);
%% Filter the data
T = .01; % seconds, .01 is for no filter.
fsize = round(T/mean(dt));
b = (1/fsize)*ones(1,fsize);
a = 1;
t_filt = filter(b,a,taxis);
t_filt = t_filt(fsize:fsize:end);
dt = diff(t_filt);
az_filt = filter(b,a,az);
az_filt = az_filt(fsize:fsize:end);
pitch_filt = filter(b,a,pitch);
pitch_filt = pitch_filt(fsize:fsize:end);
roll_filt = filter(b,a,roll);
roll_filt = roll_filt(fsize:fsize:end);

x_filt = filter(b,a,x);
x_filt = x_filt(fsize:fsize:end);
y_filt = filter(b,a,y);
y_filt = y_filt(fsize:fsize:end);
z_filt = filter(b,a,z);
z_filt = z_filt(fsize:fsize:end);
%% create the quanternions

% rotation is about [x-axis,y-axis,z-axis]???
d_theta_dt = [pitch_filt, roll_filt, az_filt];

omegaMagnitude = sqrt(sum(d_theta_dt.^2,2));
dthetaNormalized = bsxfun(@rdivide, d_theta_dt,omegaMagnitude);
dthetaNormalized(isnan(dthetaNormalized))=0;

thetaOverTwo = omegaMagnitude/2; %.*dt/2;
sinThetaOverTwo = sin(thetaOverTwo);
cosThetaOverTwo = cos(thetaOverTwo);

temp = bsxfun(@times,dthetaNormalized,sinThetaOverTwo)';
                                          
% the columns of this guy are the quanternions (i,j,k,rotation_angle)
% here (i,j,k) is the axis of rotation. 
deltaRotationVector = [temp; cosThetaOverTwo'];

%% put the acceleration measurements in a vector
A_measured = [x_filt';
              y_filt';
              z_filt'];
          
% now undo each measurement
A_qc = zeros(size(A_measured));
A_qc(:,1) = A_measured(:,1);
start = tic;
for k = 1:numel(t_filt)
    tic;
    v = [A_measured(:,k); 0]; 
    q = qconj(deltaRotationVector(:,k));
    v = qmult(q,qmult(v,qconj(q)));
    A_qc(:,k) = v(1:3);
    %fprintf('run-time step k: %0.2f\n',toc);
end
rt = toc(start);
fprintf('run-time: %f\n',rt);

figure, plot(t_filt,A_measured',t_filt,A_qc')
% figure, plot(taxis, A_measured(1,:),taxis,A_qc(1,:),'--r')
% figure, plot(taxis, A_measured(2,:),taxis,A_qc(2,:),'--r')

%%
taxis = t_filt;
X = A_qc'; %A_measured'; %
normalizedData = bsxfun(@minus,X,X(1,:));


%%
figure, plot(taxis,normalizedData)
xlabel('seconds')
ylabel('acceleration (m/s^2)')
legend('A_x','A_y','A_z')
title('accelerometer data after subtracting first record')

%% get the velocity 
V = cumtrapz(taxis,normalizedData);
figure, plot(taxis,V)
xlabel('seconds')
ylabel('velocity (m/s)')
legend('V_x','V_y','V_z')
title('velocity after subtracting first acceleration record')

%% position
P = cumtrapz(taxis,V);
figure, plot(taxis,P);
xlabel('seconds')
ylabel('position (m)')
legend('x','y','z')

dist = norm(P(end,:));
ds = sprintf('distance traveled = %f meters',dist);
s = sprintf('position after subtracting first acceleration record\n %s',ds);
title(s)


%%
% X = [x, y, z]';
% 
% mean_target = mean(X,2);
% cov_target = cov(X');
% inv_cov_target = inv(cov_target);
% re = 1e-5;
% pvalue = zeros(size(X,2),1);
% for k = 1:size(X,2)
%     t = (X(:,k) - mean_target);
%     r1 = sqrt(t'*inv_cov_target*t);
%     pvalue(k,1) = real(1 - mvnlps(mean_target, cov_target, mean_target, inv_cov_target, r1, re));
% end

% create a data set with movement too. 


% v = find(class == 1);
% mean_target = mean(Y(:,v),2);
% cov_target = cov((Y(:,v))');
% inv_cov_target = inv(cov_target);
% inv_cov_NT = inv(cov_NT);
% re = 1e-5;
% pvalue = zeros(size(coords,2),2);
% % if this errors out try using less principal components, i.e. less columns
% % of U. 
% for k = 1:size(coords,2)
%     t = (coords(:,k) - mean_target);
%     r1 = sqrt(t'*inv_cov_target*t);
%     pvalue(k,1) = real(1 - mvnlps(mean_target, cov_target, mean_target, inv_cov_target, r1, re));
%     t = (coords(:,k) - mean_NT);
%     r1 = sqrt(t'*inv_cov_NT*t);
%     pvalue(k,2) = real(1 - mvnlps(mean_NT, cov_NT, mean_NT, inv_cov_NT, r1, re));
% end















