%% artifical test for Gyroscope data. 
% This models force on an object rotating in uniform steps about an axis. 
% First the rotation is done with matrices, then it's done with 
% quanternions. The results agree. 

% no linear motion, just rotation
g = -9.8; % (m/s^2) gravity
A0 = [0; 0; g]; % acceleration at time 0. 

dt = .1;
t0 = 0;
t1 = 10;
t = t0:dt:t1;
d_theta_dt = bsxfun(@times,ones(size(t,2),3),[pi/t1,0,0]); % gyroscope radians/sec.

theta = cumtrapz(t,d_theta_dt);

R = @(phi) [  1 0 0; 
                0 cos(phi) -sin(phi);
                0 sin(phi) cos(phi)];

% A_t is what the sensor should measure         
A_t = zeros(3,size(t,2));
for k = 1:numel(t)
    A_t(:,k) = R(theta(k))*A0;
end
            
% gryoscope compensated acceleration
A_c = zeros(3,size(t,2));
for k = 1:numel(t)
    A_c(:,k) = (R(theta(k,1))')*A_t(:,k); % R' should be R inverse
end                                     % A_t would be measured acceleration

error = bsxfun(@minus,A_c,A0);

%% try using quanternions. 

% http://developer.android.com/guide/topics/sensors/sensors_motion.html
%       
%     final float dT = (event.timestamp - timestamp) * NS2S;
%     float omegaMagnitude = sqrt(axisX*axisX + axisY*axisY + axisZ*axisZ);
% 
%     // Normalize the rotation vector if it's big enough to get the axis
%     // (that is, EPSILON should represent your maximum allowable margin of error)
%     if (omegaMagnitude > EPSILON) {
%       axisX /= omegaMagnitude;
%       axisY /= omegaMagnitude;
%       axisZ /= omegaMagnitude;
%     }
% 
%     // Integrate around this axis with the angular speed by the timestep
%     // in order to get a delta rotation from this sample over the timestep
%     // We will convert this axis-angle representation of the delta rotation
%     // into a quataernion before turning it into the rotation matrix.
%     float thetaOverTwo = omegaMagnitude * dT / 2.0f;
%     float sinThetaOverTwo = sin(thetaOverTwo);
%     float cosThetaOverTwo = cos(thetaOverTwo);
%     deltaRotationVector[0] = sinThetaOverTwo * axisX;
%     deltaRotationVector[1] = sinThetaOverTwo * axisY;
%     deltaRotationVector[2] = sinThetaOverTwo * axisZ;
%     deltaRotationVector[3] = cosThetaOverTwo;
%   }
%   timestamp = event.timestamp;
%   float[] deltaRotationMatrix = new float[9];
%   SensorManager.getRotationMatrixFromVector(deltaRotationMatrix, deltaRotationVector);


omegaMagnitude = sqrt(sum(d_theta_dt.^2,2));
dthetaNormalized = bsxfun(@rdivide, d_theta_dt,omegaMagnitude);

thetaOverTwo = omegaMagnitude*dt/2;
sinThetaOverTwo = sin(thetaOverTwo);
cosThetaOverTwo = cos(thetaOverTwo);

temp = bsxfun(@times,dthetaNormalized,sinThetaOverTwo)';
                                          
% the columns of this guy are the quanternions (i,j,k,rotation_angle)
% here (i,j,k) is the axis of rotation. 
deltaRotationVector = [temp; cosThetaOverTwo'];

addpath('~/Documents/MATLAB/quaternions/')

A_measured = zeros(3,size(t,2));
A_measured(:,1) = A0; 
for k = 2:size(t,2) 
    q = deltaRotationVector(:,k-1);
    v = [A_measured(:,k-1); 0];
    tmp = qmult(q,qmult(v,qconj(q))); 
    A_measured(:,k) = tmp(1:3);
end

% now undo each measurement
A_qc = zeros(size(A_measured));
A_qc(:,1) = A_measured(:,1);
for k = 2:size(t,2)
    v = [A_measured(:,k); 0]; 
    for j = k-1:-1:1
        q = qconj(deltaRotationVector(:,j));
        v = qmult(q,qmult(v,qconj(q)));
    end
    A_qc(:,k) = v(1:3);
end

sanityCheck = A_qc - A_c;
fprintf('max error between quanternion and rotation: %e\n', max(abs(sanityCheck(:))));


%% more interesting rotation.
% 
r = 2*pi/3;

d_theta_dt = bsxfun(@times,ones(size(t,2),3),[r/t1,r/t1,r/t1]); % gyroscope radians/sec.
omegaMagnitude = sqrt(sum(d_theta_dt.^2,2));
dthetaNormalized = bsxfun(@rdivide, d_theta_dt,omegaMagnitude);

thetaOverTwo = omegaMagnitude*dt/2;
sinThetaOverTwo = sin(thetaOverTwo);
cosThetaOverTwo = cos(thetaOverTwo);

temp = bsxfun(@times,dthetaNormalized,sinThetaOverTwo)';
                                          
% the columns of this guy are the quanternions (i,j,k,rotation_angle)
% here (i,j,k) is the axis of rotation. 
deltaRotationVector = [temp; cosThetaOverTwo'];

addpath('~/Documents/MATLAB/quaternions/')

A_measured = zeros(3,size(t,2));
A_measured(:,1) = A0; 
for k = 2:size(t,2) 
    q = deltaRotationVector(:,k-1);
    v = [A_measured(:,k-1); 0];
    tmp = qmult(q,qmult(v,qconj(q)));
    A_measured(:,k) = tmp(1:3);
end

% now undo each measurement
A_qc = zeros(size(A_measured));
A_qc(:,1) = A_measured(:,1);
for k = 2:size(t,2)
    v = [A_measured(:,k); 0]; 
    for j = k-1:-1:1
        q = qconj(deltaRotationVector(:,j));
        v = qmult(q,qmult(v,qconj(q)));
    end
    A_qc(:,k) = v(1:3);
end








            