%% testing rotation about different axes. 
phi = 2*pi/3; % roation angle
qijk = ones(3,1); % axis to be rotated about.
qijk = qijk/norm(qijk);
q(1) = qijk(1)*sin(phi/2)
q(2) = qijk(2)*sin(phi/2)
q(3) = qijk(3)*sin(phi/2)
q(4) =    cos(phi/2)

%syms real a b c
a = 1; b = 0; c = 0; % The rotation takes the vector v to the vector w. 
v = [a b c 0];

w = qmult(q,qmult(v,qconj(q)))

%% remember, the rotation is ccw looking towards the origin from qijk. 
phi = 2*pi/3;
qijk = [1,0,0];
qijk = qijk/norm(qijk);
q(1) = qijk(1)*sin(phi/2)
q(2) = qijk(2)*sin(phi/2)
q(3) = qijk(3)*sin(phi/2)
q(4) =    cos(phi/2)

% syms real a b c
v = [a b c 0];

w = qmult(q,qmult(v,qconj(q)))
% pretty(w)

%%
phi = pi/2;
qijk = [0,1,0];
qijk = qijk/norm(qijk);
q(1) = qijk(1)*sin(phi/2)
q(2) = qijk(2)*sin(phi/2)
q(3) = qijk(3)*sin(phi/2)
q(4) =    cos(phi/2)

% syms real a b c
v = [a b c 0];
w = qmult(q,qmult(v,qconj(q)))
% pretty(w)
a = 0; c = 1;
v = [a b c 0];
w = qmult(q,qmult(v,qconj(q)))
w2 = qvqc(q,v(1:3))
w3 = qvrot(q,v(1:3))
w(1:3) == w2

%% find the rotation matrix

R = q2dcm(q)
qq = dcm2q(R)

% R*(v(1:3)') % this is wrong!!!
v(1:3)*R  % good
R'*(v(1:3)') % good. 








