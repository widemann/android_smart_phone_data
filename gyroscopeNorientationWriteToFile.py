#!/usr/bin/env python2.6
'''
http://www.smartphonedaq.com/accelerometer.page

startSensingTimed(Integer sensorNumber: 1 = All, 2 = Accelerometer, 3 = Magnetometer and 4 = Light,
Integer delayTime: Minimum time between readings in milliseconds)
Starts recording sensor data to be available for polling. Generates "sensors" events.
'''

import android 
import time 
import datetime

s = str(datetime.datetime.now()).split('.')[0]
ss = 'data_' + s + '.csv'
fn = ss.replace(' ', '_')

droid = android.Android() 
dt = 10 #100ms between sensings 
endTime = 3000 #sample for 3000ms 
timeSensed=0 
droid.startSensingTimed(1,dt) 
data = open(fn,'w')
data.write("t, x, y, z, az, pitch, roll\n")
while timeSensed <= endTime: 
    #print droid.sensorsReadAccelerometer().result 
    x,y,z = droid.sensorsReadAccelerometer().result
    #print droid.sensorsReadOrientation().result 
    az, pitch, roll = droid.sensorsReadOrientation().result
    #print droid.readSensors().result
    if x != None:
        data.write("%f,%f,%f,%f,%f,%f,%f\n" %(timeSensed,x,y,z,az,pitch,roll))
    time.sleep(dt/1000.0) 
    timeSensed+=dt 
droid.stopSensing()
data.close()
