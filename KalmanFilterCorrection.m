%% use Kalman filter to correct for position estimation.

%% run this script to get the data and correct for orientation. 
% There is a lot of speed-up that can be done for this. 
% It's just test code for now. 
accelerationCorrectionForOrientation;

%% get the corrected measurements and initialize Kalman parameters.
corrected_accel_meas = A_qc';
P = cov(corrected_accel_meas);

dt = mean(dt);
A = [1 dt; 0 1];
B = [dt^2/2; dt];

std_process = .05;
% Nx = std_process*B*randn
std_measure = 10; 
x = [0;0];
% measurement 
% x_meas = C x_pred + std_measure*randn 
C = [1 0];
x_meas = C*x + std_measure*randn;

% estimate the covariance matrices. 
Px = std_process^2*(B*B');% position/velocity estimated covariance matrix.
Pm = std_measure^2; %*(dt + dt^2/2); % measurement covariance (needs a (dt + dt^2/2) factor)


%%
% Define the system as a constant of 12 volts:
idx = 1;
mu = mean(corrected_accel_meas);
clear s
s.x = mu(idx); 
s.A = 1;
% Define a process noise (stdev) of 2 volts as the car operates:
s.Q = P(idx,idx); % variance, hence stdev^2
% Define the voltimeter to measure the voltage itself:
s.H = 1;
% Define a measurement error (stdev) of 2 volts:
s.R = 10; % variance, hence stdev^2
% Do not define any system input (control) functions:
s.B = 0;
s.u = 0;
% Do not specify an initial state:
s.x = nan;
s.P = nan;
% Generate random voltages and watch the filter operate.
tru=[]; % truth voltage
for tt=1:numel(taxis)
   %tru(end+1) = randn*2+12;
   s(end).z = corrected_accel_meas(tt,idx);%tru(end) + randn*2; % create a measurement
   s(end+1)=kalmanf(s(end)); % perform a Kalman filter iteration
end
figure
hold on
grid on
% plot measurement data:
hz=plot([s(1:end-1).z],'r.');
% plot a-posteriori state estimates:
hk=plot([s(2:end).x],'b-');
ht=plot(tru,'g-');
legend([hz hk ht],'observations','Kalman output','true voltage',0)
title(sprintf('Kalman Filter, Idx = %d',idx))
hold off



