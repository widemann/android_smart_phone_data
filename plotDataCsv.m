%% This scripts loads the accelerometer data and the orientation data from
% the phone and computes the final position of the phone. 

% load the data; % do this manually for now. It works better if you use 
% tab and comma separated. 
filename = '/Users/widemann/Documents/android/myScripts/data_2013-05-14_16:36:28.csv';
[t,x,y,z,az,pitch,roll] = importSL4Afile(filename)
%% This first example does not use the orientation data to correct. 
% the the final position is pretty far off from where it's expected to be. 
%% plot the acceleration
taxis = t/1e3; % originally in milliseconds
X = [x y z]; % m/s^2
normalizedData = bsxfun(@minus,X,X(1,:));

figure, plot(taxis,normalizedData)
xlabel('seconds')
ylabel('acceleration (m/s^2)')
legend('A_x','A_y','A_z')
title('accelerometer data after subtracting first record')

%% get the velocity 
V = cumtrapz(taxis,normalizedData);
figure, plot(taxis,V)
xlabel('seconds')
ylabel('velocity (m/s)')
legend('V_x','V_y','V_z')
title('velocity after subtracting first acceleration record')

%% position
P = cumtrapz(taxis,V);
figure, plot(taxis,P);
xlabel('seconds')
ylabel('position (m)')
legend('x','y','z')

dist = norm(P(end,:));
ds = sprintf('distance traveled = %f meters',dist);
s = sprintf('position after subtracting first acceleration record\n %s',ds);
title(s)


%% Try to use the orientation data to improve accuracy. 
% actually, this isn't necesary because the accelerometer gives data 
% points with respect to it's coordinate system. 
% y+ north, x+ east, z+ up
% Even if these directions are off. I don't see the accelerometer using 
% new inertial frames between measurements.

figure, plot(taxis,az)
xlabel('seconds')
title('az')
figure, plot(taxis,pitch,taxis,roll)
xlabel('seconds')
title('pitch and roll')
legend('pitch','roll')

%% Try meaning the first T seconds of data
T = 0.5;
inds = taxis <= T;
initializationMeans = mean(X(inds,:));
covM = cov(X(inds,:));

figure, scatter3(X(:,1),X(:,2),X(:,3)), hold on


normalizedData = bsxfun(@minus,X(~inds,:),initializationMeans);
tAfter = taxis(~inds);
figure, plot(tAfter,normalizedData)
xlabel('seconds')
ylabel('acceleration (m/s^2)')
legend('A_x','A_y','A_z')
title('accelerometer data after subtracting the mean over [0,T]')

%% get the velocity 
V = cumtrapz(tAfter,normalizedData);
figure, plot(tAfter,V)
xlabel('seconds')
ylabel('velocity (m/s)')
legend('V_x','V_y','V_z')
title('velocity after subtracting the mean over [0,T]')

%% position
P = cumtrapz(tAfter,V);
figure, plot(tAfter,P);
xlabel('seconds')
ylabel('position (m)')
legend('x','y','z')

dist = norm(P(end,:));
ds = sprintf('distance traveled = %f meters',dist);
s = sprintf('position after subtracting the mean over [0,T]\n %s',ds);
title(s)















